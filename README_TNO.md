# Workshop Fuzzing 2022-03-15

## What do we learn?

We'll work our way through the first exercise in [this series](https://github.com/antonio-morales/Fuzzing101/). This README is based on it, copying the relevant parts and skipping over the boring work that we already did for you.

You will:

1. Compile a target application (`pdftotext`) with instrumentation (`afl-clang-fast++`)
2. Run a fuzzer (`afl-fuzz`) on the instrumented code
3. Find a bug, analyze it with a debugger (`gdb`) and maybe even fix it.

The bug in question is a DoS-vulnerability: if someone manages to feed a PDF-file into `pdftotext` (nested somewhere in the amazing data mining service you built on top of it), the process running `pdftotext` will crash.

## What's all this stuff?

0. `README_TNO.md` - this README.
1. `xpdf-3.02` - a PDF-viewer and -toolkit, from which we'll target the `pdftotext` executable.
2. `xpdf-4.02` - a newer version of the target in which the bug has been fixed.
3. `pdf_examples` - a few seed inputs to give the fuzzer good initial coverage.
4. `crash_input.pdf` - an example of a crashing input for you to analyze in case you can't find one yourself.

Over the course of this workshop you will create some more stuff:

5. `install`- a directory with the compiled and instrumented target.
6. `install_debug` - a directory with the target compiled for debugging.
7. `out` - a directory with fuzzing results (such as crashes)
8. `output` - a placeholder file holding the output that the target produces (during normal usage)

## Get into your working environment

Work in docker to not mess with your own system, this image already contains the tooling you'll use.

```sh
# Be in the root of this repository, this will be your working directory
docker run -ti -v "$PWD":/workdir -w /workdir aflplusplus/aflplusplus
```

## Compiling with instrumentation

You should now find yourself using Docker, in the `/workdir` directory which contains the `xpdf-3.02` directory.

```sh
# Working directory environment variable
export WORKDIR=$PWD
# Set our LLVM-version
export LLVM_CONFIG="llvm-config-12"
# Go into the xpdf-directory and clean it
cd "$WORKDIR/xpdf-3.02" && make clean
# Configure the build (one command)
CC="$(which afl-clang-fast)" CXX="$(which afl-clang-fast++)" ./configure --prefix="$WORKDIR/install/"
# Actually build
make && make install
```

## Fuzzing the instrumented binary

```sh
afl-fuzz -i "$WORKDIR/pdf_examples/" -o "$WORKDIR/out/" -s 0 -- "$WORKDIR/install/bin/pdftotext" @@ "$WORKDIR/output"
```

A brief explanation of each option:

- `-i` indicates the directory where we have to put seed inputs (i.e. example PDFs).
- `-o` indicates the directory where AFL++ will store the mutated files.
- `-s` indicates the static random seed to use (this one finds you a crash relatively fast)
- `@@` AFL will substitute this with the file name of the file containing the fuzzing input.

After ~10 minutes you should have a crash (check the output in AFL's interface). You can kill AFL with `Ctrl-C` or just start a new shell from your host with `docker exec -it <container-id> bash` and set the `WORKDIR` variable again. Inputs found by AFL that crashed the target are located in `"$WORKDIR/out/crashes`.

## Analyzing the results with GDB

To analyze the crash you found in the previous step (or the provided `crash_input.pdf`) you can recompile the target binary with debugging symbols and without optimization (still in Docker):

```bash
cd "$WORKDIR/xpdf-3.02"
make clean
CFLAGS="-g -O0" CXXFLAGS="-g -O0" ./configure --prefix="$WORKDIR/install_debug/"
make && make install
```

Now run the target inside GDB:

```bash
# Load pdftotext into gdb with the crashing input (one command)
gdb --args "$WORKDIR/install/bin/pdftotext" "<path-to-crash_input.pdf>" "$WORKDIR/my_output.txt"
# Run it
(gdb) r
```

The program should produce some output ending with `Program received signal SIGSEGV, Segmentation fault.` You can get the backtrace with `bt`, which shows you the execution path at a function level that led to the error. As you can see it is quite a deep backtrace. You can try `bt -10` to see the first (bottom) 10 functions in the call stack.

### What type of problem caused the segmentation fault?

**Spoiler warning!**
The (extremely often) repeating patterns in the backtrace clearly suggest unconstrained recursion. Each function call in the recursion takes up space on the stack. This exhausts the process's memory, eventually leading to an allocation failure, at which point the libc memory allocator (top of the backtrace) throws a segmentation fault.

## Fixing the vulnerability

To look at the code you can simply use your favorite editor on your host (not in Docker).

Fixing the vulnerability requires some understanding of the code - we must preserve the intended functionality while eliminating the bug. It would be nice to find a recursion invariant that would guarantee progress in this loop, for example by requiring `Parser::getObj` or `Lexer::getObj` to advance in the input (and fail when we reach the end of the input).

Instead the developers of `xpdf` chose to resolve the issue by simply keeping track of the recursion number in every function in the recursion loop: `Parser::getObj`, `XRef::fetch`, `Dict::lookup` and `Parser::makeStream`. Once the recursion number exceeds a large-yet-arbitrary threshold (500) the recursion stops via a basic copying fallback.
